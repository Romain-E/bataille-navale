#include <stdio.h>
#include <stdlib.h>
// #include <iostream>                                                 //Pour du c++
#include <string.h>
// using namespace std;                                                //Pour du c++

void affichage(char prmTab[11][11]) {
//Fonction d'affichage d'un tableau
//paramètre : un tableau de 11 sur 11
//pas de valeur de retour
    for (int i = 0; i < 11; i++){                                   //Pour toutes les lignes
        for (int j = 0; j < 11; j++) {                              //Pour toutes les colonnes
            if (i == 0) {                                           //Si c'est la première lignes
                printf("%d", j);                                    //On affiche les numéros de colonnes
                printf(", ");
            } else {                                                //sinon
                if (j == 0) {                                       //Si c'est la première colonne
                    printf("%d", i);                                //On affiche les numéros de ligne
                    printf(", ");
                } else {                                            //Sinon
                    if ((prmTab[i][j] == 35) || (prmTab[i][j] == 45)) {
                        printf("%c", prmTab[i][j]);                 //On affiche # ou -
                        printf(", ");

                    } else {
                        printf("%d", prmTab[i][j]);                 //On affiche le reste du tableau
                        printf(", ");
                    }
                }
            }
        }
        printf("\n");                                               //Retour a la ligne a chaque ligne
    };
};

int main() {
    // int rep = 0;                                                    //???
    int nukeSent = 0;                                               //torpilles envoyées
    int hitNumber = 0;                                              //bateaux touchés
    char tabJoueur[11][11] = {0};                                   //Tableau du joueur, initialiser à 0
    char touche = '#';
    char eau = '-';
    int i = 0;                                                      //Ligne
    int j = 0;                                                      //Colonne

    char tabOrdi[11][11] = {
        {0,1,2,3,4,5,6,7,8,9,10},
        {1,0,0,0,7,7,7,7,0,0,0}, 
        {2,0,1,0,0,0,0,0,0,9,0},
        {3,0,1,6,6,6,0,0,0,9,0},
        {4,0,0,0,0,0,0,0,0,9,0},
        {5,0,0,3,0,0,0,0,0,9,0},
        {6,0,0,3,0,0,0,0,0,9,0},
        {7,8,8,8,8,0,0,0,0,0,0},
        {8,0,0,0,0,0,0,2,2,0,0},
        {9,0,0,0,0,0,0,0,0,0,0},
        {10,0,0,5,5,5,0,0,4,4,4}
    };                                                              //Tableau de l'ordi

    printf("Debut de la partie \n \n");
    affichage(tabJoueur);                                           //Affichage du tableau du joueur

    while (hitNumber < 28) {                                        //tant qu'on a pas touché 28 case, le jeu continu
        printf("Nombre de coup joue : ");
        printf("%d", nukeSent);
        printf("\n");                                               //Afichage nombre de coup
        printf("Veuillez entrer un numero de ligne : ");            //Message
        scanf("%d", &i);                                            //Saisie de la ligne

        //Vérification de la saisie ligne
        while ((i < 1) || (i > 10)){                                //Si la valeur n'est pas entre 1 et 10
            printf("valeur de ligne incorrecte \n");                //Message d'erreur
            printf("Veuillez entrer un numero de ligne : ");        //Message d'erreur
            scanf("%d", &i);                                        //Re saisie de la valeur
        }
        printf("\n tir en ligne : ");
        printf("%d", i);                                            //tir validé

        printf("\n Veuillez entrer un numero de colonne : ");       //Message
        scanf("%d", &j);                                            //Saisie de la colonne

        //Vérification de la saisie colonne
        while ((j < 1) || (j > 10)){                                //Si la valeur n'est pas entre 1 et 10
            printf("valeur de colonne incorrecte \n");              //Message d'erreur
            printf("Veuillez entrer un numero de colonne : ");      //Message d'erreur
            scanf("%d", &j);                                        //Re saisie de la valeur
        }
        printf("\n tir en colonne :");                              //Tir validé
        printf("%d", j);
        printf("\n\n");
        
        //Vérifier si la case a déja été torpillée
        if ((tabOrdi[i][j] == -1) || (tabOrdi[i][j] == -2)) {       //Si la case est déja torpillé
            printf("deja torpille \n");
        } else {                                                    //Si la case n'est pas torpillée
            nukeSent = nukeSent + 1;                                //Un coup est compté                           
            if (tabOrdi[i][j] == 0) {                               //Si on tombe à l'eau
                printf("a l'eau \n");                               //Message
                tabJoueur[i][j] = eau;                              //Affichage de "a l'eau" pour le joueur
                tabOrdi[i][j] = -1;                                 //Affichage de "a l'eau" pour l'ordi

            } else {                                                //Si on touche un bateau
                printf("Touche \n");
                hitNumber = hitNumber + 1;                          //Nb de touche + 1
                tabJoueur[i][j] = touche;                           //Affichage de "touche" pour le joueur
                tabOrdi[i][j] = -2;                                 //Affichage de "touche" pour l'ordi
            }

        }
        printf("grille joueur : \n");                               //Affichage du tableau du joueur
        affichage(tabJoueur);                                       //
        //------------ Debug-------------
        printf("\n grille ordi : \n");                              //Affichage du tableau de l'ordi !!!
        affichage(tabOrdi);                                         // !!! debug !!!!
        //-------------------------------
    }
    printf("Tout les bateaux ont etes coules. Partie terminee !!! ");//Fin de partie
}
